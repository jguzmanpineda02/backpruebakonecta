# Prueba tecnica Konecta
**Spring Boot (Java)**
A continuacion se decribe el paso a paso para el despliegue de la prieba tecnica en entorno local, cabe destacar que esta es la parte Back de la prueba.

- Primero se debe tener Java instalado en la maquina
- Luego, se clona el proyeco en el directorio de preferencia
- Después ejecuta el comando **mvn clean install** en una terminal en la raiz del proyecto
- Lo siquiente que debe hacer es ejecucar un servidor para la base de datos MySql.
- A continiacion crear una base de datos llamada **konecta** como está especificado en las configuracion del proyecto.
- Luego ejecutar el comando **mvn spring-boot:run** para correr el proyecto Spring Boot.

Y con esto tenemos nuestro proyecto corriendo de manera local.