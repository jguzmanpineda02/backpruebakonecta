package com.prueba.konecta.dto;

import java.util.List;

public class ApiResponse {

    private String message;
    private String status;
    private Object data;
    private boolean hasErrors;
    private List<ErrorDTO> errors;

    public ApiResponse(String message, String status, Object data) {
        this.message = message;
        this.status = status;
        this.data = data;
        this.hasErrors = false;
        this.errors = null;
    }

    public ApiResponse(String message, String status, List<Object> dataList) {
        this.message = message;
        this.status = status;
        this.data = dataList;
        this.hasErrors = false;
        this.errors = null;
    }

    public ApiResponse(String message, String status, boolean hasError, List<ErrorDTO> errors) {
        this.message = message;
        this.status = status;
        this.data = null;
        this.hasErrors = hasError;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public List<ErrorDTO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDTO> errors) {
        this.errors = errors;
    }

}
