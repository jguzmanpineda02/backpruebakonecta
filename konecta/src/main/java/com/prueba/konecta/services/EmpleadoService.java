package com.prueba.konecta.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.konecta.dto.ApiResponse;
import com.prueba.konecta.dto.ErrorDTO;
import com.prueba.konecta.models.Empleado;
import com.prueba.konecta.repositories.EmpleadoRepository;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

@Service
public class EmpleadoService {
    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Autowired
    private Validator validator;

    public ApiResponse all() {
        ApiResponse response = null;
        try {
            List<Empleado> empleados = empleadoRepository.findAll();
            response = new ApiResponse("OK", "success", empleados);
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }

    public ApiResponse getById(Long id) {
        ApiResponse response = null;
        try {
            Empleado empleado = empleadoRepository.findById(id).orElse(null);
            if (empleado != null) {
                response = new ApiResponse("OK", "success", empleado);
            } else {
                response = new ApiResponse("No se encontró empleado con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }

        return response;
    }

    public ApiResponse save(Empleado empleado) {
        ApiResponse response = null;
        try {
            Set<ConstraintViolation<Empleado>> violations = validator.validate(empleado);
            if (!violations.isEmpty()) {
                List<ErrorDTO> errors = new ArrayList<>();
                for (ConstraintViolation<Empleado> violation : violations) {
                    ErrorDTO error = new ErrorDTO(violation.getPropertyPath().toString(), violation.getMessage());
                    errors.add(error);
                }
                response = new ApiResponse("Error de validadcion", "error", true, errors);
            } else {
                Empleado save = empleadoRepository.save(empleado);
                if (save.getId() != null) {
                    response = new ApiResponse("Emplead cread con exito", "success", save);
                } else {
                    response = new ApiResponse("Error al crear el empleado", "error", null);
                }
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }

    public ApiResponse update(Long id, Empleado empleadoData) {
        ApiResponse response = null;
        try {
            Empleado empleado = empleadoRepository.findById(id).orElse(null);
            if (empleado != null) {
                empleado.setNombre(empleadoData.getNombre());
                empleado.setSalario(empleadoData.getSalario());
                empleadoRepository.save(empleado);
                response = new ApiResponse("Empleado actualizado con exito", "success", empleado);
            } else {
                response = new ApiResponse("No hay empleado con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }

    public ApiResponse delete(Long id) {
        ApiResponse response = null;
        try {
            Empleado empleado = empleadoRepository.findById(id).orElse(null);
            if (empleado != null) {
                empleadoRepository.delete(empleado);
                response = new ApiResponse("Empleado eliminado con exito", "success", null);
            } else {
                response = new ApiResponse("No hay empleado con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }
}
