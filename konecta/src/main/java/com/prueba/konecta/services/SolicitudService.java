package com.prueba.konecta.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.konecta.dto.ApiResponse;
import com.prueba.konecta.dto.ErrorDTO;
import com.prueba.konecta.models.Empleado;
import com.prueba.konecta.models.Solicitud;
import com.prueba.konecta.repositories.EmpleadoRepository;
import com.prueba.konecta.repositories.SolicitudRepository;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

@Service
public class SolicitudService {
    @Autowired
    private SolicitudRepository repository;
    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Autowired
    private Validator validator;

    public ApiResponse all() {
        ApiResponse response = null;
        try {
            List<Solicitud> solicitudes = repository.findAll();
            response = new ApiResponse("OK", "success", solicitudes);
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }

    public ApiResponse getById(Long id) {
        ApiResponse response = null;
        try {
            Solicitud solicitud = repository.findById(id).orElse(null);
            if (solicitud != null) {
                response = new ApiResponse("OK", "success", solicitud);
            } else {
                response = new ApiResponse("No se encontró solicitud con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }

        return response;
    }

    public ApiResponse save(Solicitud solicitud) {
        ApiResponse response = null;
        Set<ConstraintViolation<Solicitud>> violations = validator.validate(solicitud);
        if (!violations.isEmpty()) {
            List<ErrorDTO> errors = new ArrayList<>();
            for (ConstraintViolation<Solicitud> violation : violations) {
                ErrorDTO error = new ErrorDTO(violation.getPropertyPath().toString(), violation.getMessage());
                errors.add(error);
            }
            response = new ApiResponse("Error de validacion", "error", true, errors);
        } else {
            try {
                Long id = solicitud.getEmpleado().getId();
                Empleado empleado = empleadoRepository.findById(id).orElse(null);
                if (empleado != null) {
                    solicitud.setEmpleado(empleado);
                    Solicitud save = repository.save(solicitud);
                    if (save.getId() != null) {
                        response = new ApiResponse("Solicitud creada con exito", "success", save);
                    } else {
                        response = new ApiResponse("Error al crear la solicituda", "error", null);
                    }
                } else {
                    response = new ApiResponse("Empleado no encontrado", "error", null);
                }
            } catch (Exception e) {
                response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
            }

        }
        return response;
    }

    public ApiResponse update(Long id, Solicitud solicitudData) {
        ApiResponse response = null;
        try {
            Solicitud solicitud = repository.findById(id).orElse(null);
            if (solicitud != null) {
                Empleado empleado = empleadoRepository.findById(solicitudData.getEmpleado().getId()).orElse(null);
                if (empleado != null) {
                    solicitud.setEmpleado(empleado);
                    solicitud.setCodigo(solicitudData.getCodigo());
                    solicitud.setDescripcion(solicitudData.getDescripcion());
                    solicitud.setResumen(solicitudData.getResumen());
                    solicitud.setEmpleado(solicitudData.getEmpleado());
                    Solicitud update = repository.save(solicitud);
                    response = new ApiResponse("Solicitud actualizada con exito", "success", update);
                } else {
                    response = new ApiResponse("No existe el empleado con ese id", "error", null);

                }
            } else {
                response = new ApiResponse("No hay solicitud con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }

    public ApiResponse delete(Long id) {
        ApiResponse response = null;
        try {
            Solicitud solicitud = repository.findById(id).orElse(null);
            if (solicitud != null) {
                repository.delete(solicitud);
                response = new ApiResponse("Solicitud eliminada con exito", "success", null);
            } else {
                response = new ApiResponse("No hay solicitud con ese id", "error", null);
            }
        } catch (Exception e) {
            response = new ApiResponse("Ha ocurrido un error: " + e.getMessage(), "error", null);
        }
        return response;
    }
}
