package com.prueba.konecta.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.ConstraintMode;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import jakarta.persistence.*;

@Entity
@Table(name = "solicitudes")
public class Solicitud implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empleado_id", foreignKey = @ForeignKey(
        name = "id", 
        value = ConstraintMode.CONSTRAINT, 
        foreignKeyDefinition = "FOREIGN KEY (empleado_id) REFERENCES empleados(id) ON UPDATE CASCADE ON DELETE CASCADE"
        ))
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Empleado empleado;

    @Column(name = "codigo", length = 50)
    @Size(min = 1, max = 50, message = "El codigo debe tener entre 1 y 50 caracteres alfanumericos")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "El codigo debe contener caracteres alfanumericos")
    private String codigo;

    @Column(name = "descripcion", length = 50)
    @Size(min = 1, max = 50, message = "La descripcion debe tener entre 1 y 50 caracteres alfanumericos")
    @Pattern(regexp = "^[a-zA-Z ]+$", message = "La descripcion solo debe contener letras(sin caracteres especuales) y espacios")
    private String descripcion;

    @Column(name = "resumen", length = 50)
    @Size(min = 1, max = 50, message = "El resumen debe tener entre 1 y 50 caracteres alfanumericos")
    @Pattern(regexp = "^[a-zA-Z ]+$", message = "El resumen solo debe contener letras(sin caracteres especuales) y espacios")
    private String resumen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

}
