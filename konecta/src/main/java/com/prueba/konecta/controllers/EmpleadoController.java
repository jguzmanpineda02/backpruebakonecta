package com.prueba.konecta.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.konecta.dto.ApiResponse;
import com.prueba.konecta.models.Empleado;
import com.prueba.konecta.services.EmpleadoService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/empleados")
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    @GetMapping("")
    public ResponseEntity<ApiResponse> getAllEmpleados() {
        ApiResponse response = empleadoService.all();
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getById(@PathVariable Long id) {
        ApiResponse response = empleadoService.getById(id);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("")
    public ResponseEntity<ApiResponse> createEmpleado(@RequestBody Empleado empleado) {
        ApiResponse response = empleadoService.save(empleado);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> updateEmpleado(@PathVariable Long id, @RequestBody Empleado empleadoData) {
        ApiResponse response = empleadoService.update(id, empleadoData);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmpleado(@PathVariable Long id) {
        ApiResponse response = empleadoService.delete(id);
        return ResponseEntity.ok().body(response);
    }
}