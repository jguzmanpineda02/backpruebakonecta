package com.prueba.konecta.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.konecta.dto.ApiResponse;
import com.prueba.konecta.models.Solicitud;
import com.prueba.konecta.services.SolicitudService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/solicitudes")
public class SolicitudController {

    @Autowired
    private SolicitudService solicitudService;

    @GetMapping("")
    public ResponseEntity<ApiResponse> getAllSolicitudes() {
        ApiResponse response = solicitudService.all();
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getById(@PathVariable Long id) {
        ApiResponse response = solicitudService.getById(id);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("")
    public ResponseEntity<ApiResponse> createSolicutd(@RequestBody Solicitud solicitud) {
        ApiResponse response = solicitudService.save(solicitud);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> updateSolicitud(@PathVariable Long id, @RequestBody Solicitud solicitudData) {
        ApiResponse response = solicitudService.update(id, solicitudData);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSolicitud(@PathVariable Long id) {
        ApiResponse response = solicitudService.delete(id);
        return ResponseEntity.ok().body(response);
    }
}