package com.prueba.konecta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.konecta.models.Solicitud;

public interface SolicitudRepository extends JpaRepository<Solicitud, Long> {

}
